<?php


namespace nbykov\CalculateBundle\Tests\Unit\Controller;


use nbykov\CalculateBundle\Controller\CalculateController;
use nbykov\CalculateBundle\Entity\LocalExpression;
use nbykov\CalculateBundle\Service\CalculateServiceInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CalculateControllerTest extends TestCase
{

    private const EXAMPLES = [
        'valid' => [
            ['100^2/2*(10-6*(9+10))-7*(9+1)', -520070],
            ['100^2/2.5*(10.5-6*(12-2))-7*(9+1)', -198070],
            ['1+3/2', 2.5],
            ['1+1', 2]
        ],
        'invalid' => [
            '2*4/(2-2)', // divide by zero
            '2*4/(2-a)', // letter-symbol
            '2*4/(2-1))', // extra closing bracket
            '(2*4/(2-1)' // extra opening bracket
        ]
    ];

    private function getCalculateService()
    {
        return new class implements CalculateServiceInterface
        {
            public function calculate(string $expression): float
            {
                $expression = new LocalExpression($expression);
                return $expression->calculate();
            }
        };
    }

    private function getRequest(string $expression)
    {
        $request = $this->getMockBuilder(Request::class)
            ->setMethods(['getContent', 'getContentType'])
            ->disableOriginalConstructor()
            ->getMock();

        $request->method('getContent')
            ->willReturn(json_encode(['expression' => $expression]));
        $request->method('getContentType')
            ->willReturn('json');

        return $request;
    }

    private function getController()
    {
        $controller = new CalculateController();
        $container = $this->getMockBuilder(Container::class)
            ->disableOriginalConstructor()
            ->setMethods(['has'])
            ->getMock();

        $container->method('has')->willReturn(false);
        $controller->setContainer($container);
        $controller->setCalculatorService($this->getCalculateService());

        return $controller;
    }

    public function testCalculateValidExpressions()
    {
        $controller = $this->getController();
        foreach (self::EXAMPLES['valid'] as $expression) {
            $request = $this->getRequest($expression[0]);
            $this->assertEquals(new JsonResponse(
                ['result' => $expression[1]]
            ), $controller->calculate($request));
        }
    }

    public function testCalculateInvalidExpressions() {
        $controller = $this->getController();
        foreach (self::EXAMPLES['invalid'] as $expression) {
            $request = $this->getRequest($expression);
            $this->assertEquals(500, $controller->calculate($request)->getStatusCode());
        }
    }
}
