<?php


namespace Tests\Unit\Entity;


use nbykov\CalculateBundle\Entity\Exceptions\ExpressionFormatException;
use nbykov\CalculateBundle\Entity\Exceptions\LocalExpressionZeroDivide;
use nbykov\CalculateBundle\Entity\LocalExpression;
use PHPUnit\Framework\TestCase;

class LocalExpressionTest extends TestCase
{

    private const VALID_EXAMPLES = [
        ['100^2/2*(10-6*(9+10))-7*(9+1)', -520070],
        ['100^2/2.5*(10.5-6*(12-2))-7*(9+1)', -198070],
        ['1+3/2', 2.5],
        ['1+1', 2]
    ];

    private const INVALID_EXAMPLES = [
        '100^2/2*(10-6*(9+10))-7*(9+1))', // extra closing bracket
        '(100^2/2*(10-6*(9+10))-7*(9+1)', // extra opening bracket
        '(100^2/2*(10+a-6*(9+10))-7*(9+1)', // letter in the middle
        '(2+3)/(2-2)' // division on zero
    ];

    public function testLocalExpressionValid()
    {
        foreach (self::VALID_EXAMPLES as $variant) {
            $expression = new LocalExpression($variant[0]);
            $result = $expression->calculate();
            $this->assertEquals($variant[1], $result);
        }
    }

    public function testLocalExpressionInValidExtraClosingBracket()
    {
        $this->expectException(ExpressionFormatException::class);
        $expression = new LocalExpression(self::INVALID_EXAMPLES[0]);
    }

    public function testLocalExpressionInValidExtraOpeningBracket()
    {
        $this->expectException(ExpressionFormatException::class);
        $expression = new LocalExpression(self::INVALID_EXAMPLES[1]);
    }

    public function testLocalExpressionInValidUnexpectedSymbol()
    {
        $this->expectException(ExpressionFormatException::class);
        $expression = new LocalExpression(self::INVALID_EXAMPLES[2]);
    }

    public function testLocalExpressionInValidDivisionByZero()
    {
        $this->expectException(LocalExpressionZeroDivide::class);
        $expression = new LocalExpression(self::INVALID_EXAMPLES[3]);
        $expression->calculate();
    }
}