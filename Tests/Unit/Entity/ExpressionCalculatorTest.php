<?php


namespace Tests\Unit\Entity;


use nbykov\CalculateBundle\Entity\ExpressionCalculator;
use nbykov\CalculateBundle\Entity\LocalExpression;
use PHPUnit\Framework\TestCase;
use function Sodium\add;

class ExpressionCalculatorTest extends TestCase
{

    private const EXAMPLES = [
        'basic_examples' => [
            'add' => [
                'operands' => ['(10+4)/2', '(24-6)/9'],
                'result' => 9
            ],
            'subtract' => [
                'operands' => ['(10+4)/2', '(24-6)/9'],
                'result' => 5
            ],
            'divide' => [
                'operands' => ['(10+4)*2', '(24-3)/3'],
                'result' => 4
            ],
            'multiply' => [
                'operands' => ['(10+4)*2', '(24-3)/3'],
                'result' => 196
            ],
            'pow' => [
                'operands' => ['(10+4)*2', '(24-3)/3'],
                'result' => 13492928512
            ],
        ],
        'complex_examples' => [
            [
                'operands' => [
                    '10+2' => '',
                    '(2+3)*6' => '/'
                ],
                'result' => 0.4
            ], [
                'operands' => [
                    '100^2/2*(10-6*(9+10))-7*(9+1)' => '',
                    '100^2/2.5*(10.5-6*(12-2))-7*(9+1)' => '-'
                ],
                'result' => -322000
            ], [
                'operands' => [
                    '10+2' => '',
                    '(2+3)*6' => '*'
                ],
                'result' => 360
            ], [
                'operands' => [
                    '10+2' => '',
                    '(3-1)*2' => '^'
                ],
                'result' => 20736
            ], [
                'operands' => [
                    '100^2/2*(10-6*(9+10))-7*(9+1)' => '',
                    '100^2/2.5*(10.5-6*(12-2))-7*(9+1)' => '+'
                ],
                'result' => -718140
            ]
        ],
        'pop' => ['(10+4)/2', '(24-6)/9', '(10+4)*2', '(24-3)/3']
    ];

    public function testExpressionCalculatorValid()
    {
        foreach (self::EXAMPLES['complex_examples'] as $example) {
            $calculator = new ExpressionCalculator();
            foreach ($example['operands'] as $expression => $operation) {
                switch ($operation) {
                    case '+':
                        $calculator->add(new LocalExpression($expression));
                        break;

                    case '/':
                        $calculator->divide(new LocalExpression($expression));
                        break;

                    case '-':
                        $calculator->subtract(new LocalExpression($expression));
                        break;

                    case '*':
                        $calculator->multiply(new LocalExpression($expression));
                        break;

                    case '^':
                        $calculator->pow(new LocalExpression($expression));
                        break;

                    default:
                        $calculator->add(new LocalExpression($expression));
                        break;
                }
            }
            $this->assertEquals($example['result'], $calculator->calculate());
        }
    }

    public function testAdd()
    {
        $calculator = new ExpressionCalculator();
        $result = $calculator->add(new LocalExpression(self::EXAMPLES['basic_examples']['add']['operands'][0]))
            ->add(new LocalExpression(self::EXAMPLES['basic_examples']['add']['operands'][1]))
            ->calculate();

        $this->assertEquals(self::EXAMPLES['basic_examples']['add']['result'], $result);
    }

    public function testSubtract()
    {
        $calculator = new ExpressionCalculator();
        $result = $calculator->add(new LocalExpression(self::EXAMPLES['basic_examples']['subtract']['operands'][0]))
            ->subtract(new LocalExpression(self::EXAMPLES['basic_examples']['subtract']['operands'][1]))
            ->calculate();

        $this->assertEquals(self::EXAMPLES['basic_examples']['subtract']['result'], $result);
    }

    public function testDivide()
    {
        $calculator = new ExpressionCalculator();
        $result = $calculator->add(new LocalExpression(self::EXAMPLES['basic_examples']['divide']['operands'][0]))
            ->divide(new LocalExpression(self::EXAMPLES['basic_examples']['divide']['operands'][1]))
            ->calculate();

        $this->assertEquals(self::EXAMPLES['basic_examples']['divide']['result'], $result);
    }

    public function testMultiply()
    {
        $calculator = new ExpressionCalculator();
        $result = $calculator->add(new LocalExpression(self::EXAMPLES['basic_examples']['multiply']['operands'][0]))
            ->multiply(new LocalExpression(self::EXAMPLES['basic_examples']['multiply']['operands'][1]))
            ->calculate();

        $this->assertEquals(self::EXAMPLES['basic_examples']['multiply']['result'], $result);
    }

    public function testPow()
    {
        $calculator = new ExpressionCalculator();
        $result = $calculator->add(new LocalExpression(self::EXAMPLES['basic_examples']['pow']['operands'][0]))
            ->pow(new LocalExpression(self::EXAMPLES['basic_examples']['pow']['operands'][1]))
            ->calculate();

        $this->assertEquals(self::EXAMPLES['basic_examples']['pow']['result'], $result);
    }

    public function testExpressionCalculatorPop()
    {
        $operands = self::EXAMPLES['pop'];
        shuffle($operands);

        array_walk($operands, function (&$operand) {
            $operand = new LocalExpression($operand);
        });

        $operations = ['+', '-', '*', '/', '^'];
        shuffle($operations);

        $calculator_a = new ExpressionCalculator();
        $calculator_b = new ExpressionCalculator();

        $current_operation = '+';
        while ($operand = array_shift($operands)) {
            $use_calculator_b = count($operands) > 0;
            switch ($current_operation) {
                case '+':
                    $calculator_a->add($operand);
                    if ($use_calculator_b)
                        $calculator_b->add($operand);
                    break;

                case '-':
                    $calculator_a->subtract($operand);
                    if ($use_calculator_b)
                        $calculator_b->subtract($operand);
                    break;

                case '*':
                    $calculator_a->multiply($operand);
                    if ($use_calculator_b)
                        $calculator_b->multiply($operand);
                    break;

                case '/':
                    $calculator_a->divide($operand);
                    if ($use_calculator_b)
                        $calculator_b->divide($operand);
                    break;

                case '^':
                    $calculator_a->pow($operand);
                    if ($use_calculator_b)
                        $calculator_b->pow($operand);
                    break;
            }

            $current_operation = $operations[array_rand($operations)];
        }

        $result_b = $calculator_b->calculate();
        $result_a = $calculator_a->pop()->calculate();

        $this->assertEquals($result_b, $result_a);
    }
}
