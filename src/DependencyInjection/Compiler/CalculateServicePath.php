<?php


namespace nbykov\CalculateBundle\DependencyInjection\Compiler;


use nbykov\CalculateBundle\Controller\CalculateController;
use nbykov\CalculateBundle\Service\CalculateServiceInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class CalculateServicePath implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(CalculateController::class))
            return;

        $controller = $container->findDefinition(CalculateController::class);
        foreach (array_keys($container->findTaggedServiceIds(CalculateServiceInterface::TAG)) as $service)
            $controller->addMethodCall('setCalculatorService', [new Reference($service)]);
    }
}
