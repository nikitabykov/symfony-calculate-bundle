<?php


namespace nbykov\CalculateBundle\Entity;

interface ExpressionCalculatorInterface extends ExpressionInterface
{

    /**
     * '+' operation
     * @param ExpressionInterface $expression
     * @return ExpressionCalculatorInterface
     */
    public function add(ExpressionInterface $expression): ExpressionCalculatorInterface;

    /**
     * '-' operation
     * @param ExpressionInterface $expression
     * @return ExpressionCalculatorInterface
     */
    public function subtract(ExpressionInterface $expression): ExpressionCalculatorInterface;

    /**
     * '*' operation
     * @param ExpressionInterface $expression
     * @return ExpressionCalculatorInterface
     */
    public function multiply(ExpressionInterface $expression): ExpressionCalculatorInterface;

    /**
     * '/' operation
     * @param ExpressionInterface $expression
     * @return ExpressionCalculatorInterface
     */
    public function divide(ExpressionInterface $expression): ExpressionCalculatorInterface;

    /**
     * '^' operation
     * @param ExpressionInterface $expression
     * @return ExpressionCalculatorInterface
     */
    public function pow(ExpressionInterface $expression): ExpressionCalculatorInterface;

    /**
     * Removes last operand
     * @return ExpressionCalculatorInterface
     */
    public function pop(): ExpressionCalculatorInterface;
}
