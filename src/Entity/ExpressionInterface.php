<?php


namespace nbykov\CalculateBundle\Entity;


interface ExpressionInterface
{
    /**
     * Calculates result
     * @return float
     */
    public function calculate(): float;
}
