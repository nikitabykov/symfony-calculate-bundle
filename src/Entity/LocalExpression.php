<?php


namespace nbykov\CalculateBundle\Entity;

use nbykov\CalculateBundle\Entity\Exceptions\ExpressionFormatException;
use nbykov\CalculateBundle\Entity\Exceptions\LocalExpressionZeroDivide;

class LocalExpression implements ExpressionInterface
{

    private const WEIGHTS = [
        '(' => -1,
        ')' => 0,
        '-' => 1,
        '+' => 1,
        '/' => 2,
        '*' => 2,
        '^' => 3
    ];

    private $expression = '';

    /**
     * {@inheritdoc}
     */
    public function __construct(string $expression)
    {
        $patterns = [
            '/^[\-\+\*\^\/\d\.\(\)]+$/', // expression consists only from digits, operations sna brackets
            '/^[\(|\d]+/', // starting with digit or opening bracket and finishing on digit or closing bracket
            '/[\d|\)]+$/' // starting with digit or opening bracket and finishing on digit or closing bracket
        ];

        foreach ($patterns as $pattern) {
            $matches = [];
            preg_match($pattern, $expression, $matches);
            if (empty($matches)) {
                throw new ExpressionFormatException('Expression should consist of digits, dots, brackets and math operations');
            }
        }

        $brackets = [];
        for ($i = 0; $i < strlen($expression); $i++) {
            if ($expression[$i] == '(') array_push($brackets, '(');
            elseif ($expression[$i] == ')') {
                if (empty($brackets)) {
                    throw new ExpressionFormatException('Wrong brackets number');
                }
                array_pop($brackets);
            }
        }
        if (!empty($brackets)) {
            throw new ExpressionFormatException('Wrong brackets number');
        }

        $this->expression = $expression;
    }

    /**
     * {@inheritdoc}
     */
    private function convertToPolandReverseNotation(string $expression): array
    {

        $operations = []; // operations stack
        $result = []; // result array

        $expression = trim(preg_replace('/[0-9\.]+|[\*\-\+\/\^\(\)]/', ' ${0} ', $expression));
        $expression = preg_replace('/\s{2,}/', ' ', $expression);
        $expression = explode(' ', $expression);

        for ($i = 0; $i < count($expression); $i++) {
            $symbol = $expression[$i];
            if (key_exists($symbol, self::WEIGHTS)) { // operator or bracket
                $symbol_weight = self::WEIGHTS[$symbol];
                if ($symbol_weight > 0) { // operator
                    while ($last_operator = end($operations)) {
                        $last_operator_weight = self::WEIGHTS[$last_operator];
                        if ($symbol_weight <= $last_operator_weight) {
                            $last_operator = array_pop($operations);
                            $result[] = $last_operator;
                        } else
                            break;
                    }
                    $operations[] = $symbol;
                } elseif ($symbol_weight == 0) { // closing bracket
                    while ($last_operator = array_pop($operations)) {
                        $last_operator_weight = self::WEIGHTS[$last_operator];
                        if ($last_operator_weight < 0) // we came to opening brake
                            break;
                        $result[] = $last_operator;
                    }
                } else
                    $operations[] = $symbol;
            } else // digit
                $result[] = $symbol;
        }

        while ($operator = array_pop($operations))
            $result[] = $operator;

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    private function calculatePolandReverseNotation(array $expression): float
    {
        $result = [];

        for ($i = 0; $i < count($expression); $i++) {
            $value = $expression[$i];
            if (key_exists($value, self::WEIGHTS)) {

                $operand_b = array_pop($result);
                $operand_a = array_pop($result);

                switch ($value) {
                    case '-':
                        $res = $operand_a - $operand_b;
                        break;

                    case '+':
                        $res = $operand_a + $operand_b;
                        break;

                    case '/':
                        if ($operand_b == 0)
                            throw new LocalExpressionZeroDivide('Division on "0" prohibited');
                        $res = $operand_a / $operand_b;
                        break;

                    case '*':
                        $res = $operand_a * $operand_b;
                        break;

                    case '^':
                        $res = pow($operand_a, $operand_b);
                        break;
                }
                array_push($result, $res);
            } else {
                array_push($result, $value);
            }
        }

        return implode('', $result);
    }

    /**
     * {@inheritdoc}
     */
    public function calculate(): float
    {
        $expression = $this->convertToPolandReverseNotation($this->expression);
        return $this->calculatePolandReverseNotation($expression);
    }
}
