<?php


namespace nbykov\CalculateBundle\Entity;


class ExpressionCalculator implements ExpressionCalculatorInterface
{

    private $expressions = [];

    /**
     * {@inheritdoc}
     */
    public function add(ExpressionInterface $expression): ExpressionCalculatorInterface
    {
        array_push($this->expressions, [
            $expression,
            '+'
        ]);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function subtract(ExpressionInterface $expression): ExpressionCalculatorInterface
    {
        array_push($this->expressions, [
            $expression,
            '-'
        ]);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function multiply(ExpressionInterface $expression): ExpressionCalculatorInterface
    {
        array_push($this->expressions, [
            $expression,
            '*'
        ]);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function divide(ExpressionInterface $expression): ExpressionCalculatorInterface
    {
        array_push($this->expressions, [
            $expression,
            '/'
        ]);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function pow(ExpressionInterface $expression): ExpressionCalculatorInterface
    {
        array_push($this->expressions, [
            $expression,
            '^'
        ]);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function pop(): ExpressionCalculatorInterface
    {
        array_pop($this->expressions);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function calculate(): float
    {
        $result = 0;

        foreach ($this->expressions as $expression_description) {
            $value = $expression_description[0]->calculate();
            switch ($expression_description[1]) {
                case '+':
                    $result = $result + $value;
                    break;
                case '-':
                    $result = $result - $value;
                    break;
                case '*':
                    $result = $result * $value;
                    break;
                case '/':
                    $result = $result / $value;
                    break;
                case '^':
                    $result = pow($result, $value);
                    break;

                default:
                    $result = $value;
                    break;
            }
        }

        return $result;
    }
}
