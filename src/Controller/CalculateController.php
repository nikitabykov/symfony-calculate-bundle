<?php


namespace nbykov\CalculateBundle\Controller;


use nbykov\CalculateBundle\Service\CalculateServiceInterface;
use nbykov\CalculateBundle\Service\Exception\CalculateServiceException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CalculateController extends AbstractController
{
    private $calculate_service = null;

    /**
     * Setting service for calculations
     * @param CalculateServiceInterface $calculate_service
     */
    public function setCalculatorService(CalculateServiceInterface $calculate_service)
    {
        $this->calculate_service = $calculate_service;
    }

    /**
     * @Route("/calculate")
     * @param Request $request
     * @return JsonResponse
     */
    public function calculate(Request $request): JsonResponse
    {
        if ($request->getContentType() != 'json' || !$request->getContent())
            throw new CalculateServiceException('Request should be in JSON-format');

        if ($this->calculate_service === null)
            throw new CalculateServiceException('No `CalculateService` defined');

        $request_body = json_decode($request->getContent());

        try {
            $result = $this->calculate_service->calculate($request_body->expression);
            return $this->json(['result' => $result]);
        } catch (\LogicException $e) {
            return $this->json(['message' => $e->getMessage()], $e->getCode() ? $e->getCode() : 500);
        }
    }
}
