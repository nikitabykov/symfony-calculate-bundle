<?php


namespace nbykov\CalculateBundle\Service;


interface CalculateServiceInterface
{
    public const TAG = 'calculate.service';

    /**
     * Calculates result of expression
     * @param string $expression
     * @return float
     */
    public function calculate(string $expression): float;
}
