<?php


namespace nbykov\CalculateBundle;


use nbykov\CalculateBundle\DependencyInjection\Compiler\CalculateServicePath;
use nbykov\CalculateBundle\Service\CalculateServiceInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CalculateBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new CalculateServicePath());

        $container->registerForAutoconfiguration(CalculateServiceInterface::class)
            ->addTag(CalculateServiceInterface::TAG);
    }
}
