# Symfony Calculate Bundle

## Installation

1\. Install bundle via composer: `composer require nbykov/calculate-bundle`

2\. Add route:
```yaml
calculate:
    resource: "@CalculateBundle/Controller/CalculateController.php"
    prefix: /
    type: annotation
```

3\. Create your own service like (`src/Service/Calculate.php`):

```php
<?php

namespace App\Service;

use nbykov\CalculateBundle\Entity\LocalExpression;
use nbykov\CalculateBundle\Service\CalculateServiceInterface;

class CalculateService implements CalculateServiceInterface
{
    public function calculate(string $expression): float
    {
        $expression = new LocalExpression($expression);
        return $expression->calculate();
    }
}
```


## Usage
Simply send a POST-request to `/calculate` like:
```
POST /calculate HTTP/1.1
Host: <your host>
Content-Type: application/json
Accept: */*
Cache-Control: no-cache
Accept-Encoding: gzip, deflate
Content-Length: 34
Connection: keep-alive
cache-control: no-cache

{
	"expression" : "(10*2-6)/3+5"
}
```

And you will receive a response like:

```json
{
    "message": "Expression should consist of digits, dots, brackets and math operations"
}
```

or:

```json
{
    "result": 8
}
```

## Extra

You can calculate result of expressions chain:

```php
$calculator  = new ExpressionCalculator();
$result = $calculator->add(new LocalExpression('...'))
    ->divide(new LocalExpression('...'))
    ->multiply(new LocalExpression('...'))
    ->calculate();
```
